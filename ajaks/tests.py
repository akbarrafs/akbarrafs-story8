from django.test import TestCase, LiveServerTestCase
# from .models import 
from .views import index
# from .forms import
from django.urls import resolve


# Create your tests here.
class StoryDelapanUnitTest(TestCase):
    def test_landing(self):
        response = self.client.get("/")
        self.assertTemplateUsed(response, 'ajaks.html')
        self.assertEqual(response.status_code, 200)
    
    def test_landing_views(self):
        response = resolve("/")
        self.assertEqual(response.func, index)
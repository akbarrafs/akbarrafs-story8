$(document).on({
    ajaxStart: function () {
        $('.none').css('display', 'block');
    },
    ajaxStop: function () {
        $('.none').css('display', 'none');
    }
});

$(document).ready(function () {
    $('#myform').submit(function (event) {
        let searchItem = $('.searchbox').val();
        console.log(searchItem);
        let urlItem = "https://www.googleapis.com/books/v1/volumes?q=" + searchItem;

        $.ajax({
            async: true,
            type: 'GET',
            url: urlItem,
            dataType: 'json',
            success: function(searchRes) {
                console.log(searchRes)
                $('.status-container').next().addClass('none');
                $('.status-container').removeClass('none');
                let bookShelf = searchRes.items;
                console.log(bookShelf)

                $('tbody').empty();

                for (i = 0; i < 10; i++) {
                    let book = bookShelf[i].volumeInfo;
                    var num = $('<td>').text(i + 1);
                    var name = $('<td>').text(book.title);

                    if ('authors' in book == false) var author = $('<td>').text("-");
                    else var author = $('<td>').text(book.authors);

                    if ('imageLinks' in book == false)
                        var img = $('<td>').text("-");

                    else {
                        if ('smallThumbnail' in book.imageLinks == false)
                            var img = $('<td>').append($('<img>').attr({
                                'src': book.imageLinks.thumbnail
                            }));
                        else
                            var img = $('<td>').append($('<img>').attr({
                                'src': book.imageLinks.smallThumbnail
                            }));
                    }

                    var tr = $('<tr>').append(num, name, author, img);

                    $('tbody').append(tr);

                }
            },
            // error: function () {
            //     $('.status-container').next().addClass('none');
            //     alert("There's a problem between you and her.");
            // },
            type: 'GET',
        });
        event.preventDefault();
    })
});


